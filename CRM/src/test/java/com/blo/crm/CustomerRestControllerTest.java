package com.blo.crm;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.blo.crm.controller.CustomerController;
import com.blo.crm.entity.Customer;
import com.blo.crm.entity.exception.CustomerNotFound;
import com.blo.crm.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CustomerController.class)
public class CustomerRestControllerTest {

	@MockBean
	private CustomerService customerService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	Customer mockCustomer;
	
	@Before
	public void setup() {
		this.mockCustomer = new Customer(1L, "firstName", "lastName", "email@email.com");
	}

	private String getRootUrl() {
		return "/api/v1/customers";
	}
	
	@Test
	public void customerDoesNotExist() throws Exception {
		when(customerService.findCustomerById(0)).thenThrow(new CustomerNotFound());
		this.mockMvc.perform(get(getRootUrl() + "/0"))
		.andExpect(status().isNotFound());
				
	}
	
	@Test
	public void testGetAllCustomers() throws Exception {
		List<Customer> allCustomers = Arrays.asList(mockCustomer);
		given(customerService.getAllCustomers()).willReturn(allCustomers);

		// when + then
		this.mockMvc.perform(get(getRootUrl()))
		.andExpect(status().isOk()).andExpect(content()
				.json("[{'id': 1," + "'firstName': 'firstName'," + "'lastName': 'lastName', " + "'email' : 'email@email.com'}]"));

	}
	

	@Test
	public void testGetCustomerById() throws Exception {
		given(customerService.findCustomerById(1)).willReturn(mockCustomer);

		this.mockMvc.perform(get(getRootUrl() + "/1")).andExpect(status().isOk())
				.andExpect(jsonPath("_links", notNullValue())).andExpect(content()
						.json("{'id': 1," + "'firstName': 'firstName'," + "'lastName': 'lastName', " + "'email' : 'email@email.com'}"));

	}
	
	@Test
	public void testPostCustomer() throws Exception {
			//this commented code works but we trying the expanded approach
//		given(userService.createUser(mockUser)).willReturn(mockUser);
//		this.mockMvc.perform(post(getRootUrl() + "/users").contentType("application/json")
//				// .param("sendWelcomeMail", "true")
//				.content(objectMapper.writeValueAsString(mockUser))).andExpect(status().isCreated());

		RequestBuilder requestBuilder= MockMvcRequestBuilders.post(getRootUrl() )
				.accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(mockCustomer)) //this LOC converts mockCustomer to JSON but also add '.contentType' to confirm the MediaType
				.contentType(MediaType.APPLICATION_JSON);
		
		MvcResult mvcResult= mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response= mvcResult.getResponse();
		
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		
	}
	
	@Test
	public void testDeleteCustomer() throws Exception {
		given(customerService.findCustomerById(1)).willReturn(mockCustomer);
		this.mockMvc.perform(delete(getRootUrl() + "/1")).andExpect(status().is2xxSuccessful()).andExpect(
				content().json("{'Customer deleted':{'rel':'ALL CUSTOMERS','href':'http://localhost/api/v1/customers'}}", true));
				//.andDo(print())
		
	}
	
//	@Test
//	public void testPutCustomer() throws Exception {
//		given(customerService.findCustomerById(1)).willReturn(mockCustomer);
//
//		mockMvc.perform(put(getRootUrl() + "/1")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(objectMapper.writeValueAsString(
//						new Customer( "testFirstName", "testLastName", "test@email.com"))
//						)) //update with new customer
//		
//		.andExpect(status().isOk())
//		.andDo(print());
//
//	}
//	
	
//------------------TEST FOR OTHER EXCEPTION HANDLERS---------------------------//
	//Not testing ConstarintViolation because we're not using @Validated
	//Not testing MissingServletRequestParameter because we're not using required params
	// test for argument mismatch
	@Test
	public void whenMethodArgumentMismatch_thenBadRequest() throws Exception {

		mockMvc.perform(get(getRootUrl() + "/test"))
				.andExpect(content().string(containsString("should be of type"))).andExpect(status().isBadRequest());
				//.andDo(print());

	}
	
	@Test
	public void whenHttpRequestMethodNotSupported_thenMethodNotAllowed() throws Exception {
		 mockMvc.perform(patch(getRootUrl()+"/1")) //patch that is if patch wasnt supported
		 .andExpect(status().isMethodNotAllowed())
		 .andExpect(content().string(containsString("not supported")));
		// .andDo(print());
		
	}
	
	@Test
	public void whenMethodArgumentNotValid_thenBadRequest() throws Exception{
		mockMvc.perform(post(getRootUrl())
				.content(objectMapper.writeValueAsString(new Customer("firstName", "lastName", "email"))) //email is wrong
				.contentType("application/json")
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest())
		.andExpect(jsonPath("$.message").value("Method Arguments Are Invalid"));
		//.andDo(print());
		
	}
	
	
	@Test
	public void whenHttpMediaTypeNotSupported_thenBadRequest() throws Exception{
		mockMvc.perform(post(getRootUrl())
				.content(objectMapper.writeValueAsString(mockCustomer))
				.contentType(MediaType.APPLICATION_XML))
		.andExpect(status().isUnsupportedMediaType());
		//.andDo(print());
		
	}
	
	@Test
	public void whenPageNotFound_thenNotFound() throws Exception {
		mockMvc.perform(get(getRootUrl() + "/3/test")) // wrong URL
				.andExpect(status().isNotFound());
	}


	
	
}
