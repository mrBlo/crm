package com.blo.crm.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.crm.entity.Customer;
import com.blo.crm.entity.exception.CustomerNotFound;
import com.blo.crm.repository.CustomerRepository;
import com.blo.crm.service.CustomerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;



@RestController
@RequestMapping(value = "/api/v1")
@Tag(name = "Customer", description = "the Customer API") //OpenApi annotation
@CrossOrigin(origins ="http://localhost:3000") //enable CORS on all methods of this Controller to this origin 
//, maxAge = 3600) //maxAge of CORS ie. 60 minutes 
public class CustomerController {
	
	@Autowired
	CustomerService customerService;
	
	
	@Operation(summary = "Get All Customers", description = "", tags = { "customer" })
	 @ApiResponses(value = {
		        @ApiResponse(responseCode = "200", description = "successful operation") })	
	@GetMapping(value = "/customers")
	public List<Customer> getAll(){
		List<Customer> allCustomers= customerService.getAllCustomers();
		for(Customer customer: allCustomers) {
			customer.add(linkTo(CustomerRepository.class).slash("customers").slash(customer.getId()).withSelfRel());
		}
		
		return allCustomers;
	}
	
	
	

	  @Operation(summary = "Find customer by id",
	    description = "Also returns a link to retrieve all customers with rel - all-customers",
	    tags = { "customer" })
	  @ApiResponses(value = {
		        @ApiResponse(responseCode = "200", description = "successful operation"),
		        @ApiResponse(responseCode = "404", description = "Customer not found") })
	@GetMapping(value="/customers/{id}")
	public Customer getCustomer(@PathVariable (value = "id") long id ){
		Customer customer= customerService.findCustomerById(id);
			if (customer==null) throw new CustomerNotFound();
			 //implementing HATEOAS GET allCustomers link here
			 Link allCustomersLink = linkTo(methodOn(CustomerController.class).getAll()).withRel("ALL CUSTOMERS");
			 customer.add(allCustomersLink);
		return customer;
	}
	
	
	  
	  
	 @Operation(
	          summary = "Create a new customer",
	          description = "Use this endpoint to create a new customer in the backend",
	          tags = { "customer" },
	          responses= {
	              @ApiResponse(responseCode = "201", description = "Contact created"),
	              @ApiResponse(responseCode = "400", description = "Bad Request")
//	              @ApiResponse(responseCode = "500", description = "Server Error")
	          }
	     )
	@PostMapping(value="/customers" , consumes =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> post(@Valid @RequestBody Customer customer){
		 customerService.createCustomer(customer);
		return new ResponseEntity<>(HttpStatus.CREATED);	
	}
	 
	 
	 
	 @Operation(summary = "Deletes an existing customer"
			 , description = "Also returns a link to retrieve all customers with rel - all-customers"
			 , tags = { "customer" })
	    @ApiResponses(value = { 
	        @ApiResponse(responseCode = "200", description = "successful operation"),
	        @ApiResponse(responseCode = "404", description = "Customer not found") })
	 @DeleteMapping(value="/customers/{id}")
	  public Map<String, Link> delete(@PathVariable(value = "id") long id){
	    Customer customer = customerService.findCustomerById(id);
	    if (customer==null) throw new CustomerNotFound();
	    customerService.deleteCustomer(customer);
	  //implementing HATEOAS GET allCustomers link here
		 Link allCustomersLink = linkTo(methodOn(CustomerController.class).getAll()).withRel("ALL CUSTOMERS");
	    Map<String, Link> response = new HashMap<>();
	    response.put("Customer deleted",allCustomersLink);
	    return response;
	  }
	 
	 
	 
	 
	 
	 @Operation(summary = "Update an existing customer"
			 , description = "Also returns a link to retrieve updated customer with rel - self"
			 , tags = { "customer" })
	 @ApiResponses(value = {
		        @ApiResponse(responseCode = "200", description = "successful operation"),
		        @ApiResponse(responseCode = "400", description = "Bad Request"),
		        @ApiResponse(responseCode = "404", description = "Customer not found"),
		        @ApiResponse(responseCode = "405", description = "Validation exception") })
	 @PutMapping(value="/customers/{id}", consumes =  MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Customer> put(@Valid @RequestBody Customer newCustomer, @PathVariable (value = "id")  long id) {
			
			Customer customer = customerService.findCustomerById(id); 
			if(customer==null) throw new CustomerNotFound();
		
			customer.setFirstName(newCustomer.getFirstName());
			customer.setLastName(newCustomer.getLastName());
			customer.setEmail(newCustomer.getEmail());
			Customer updatedCustomer = customerService.createCustomer(customer);
			//implementing HATEOAS self link here
			 Link selfLink = linkTo(CustomerController.class).slash("customers").slash(updatedCustomer.getId()).withSelfRel();
			 updatedCustomer.add(selfLink);
			return ResponseEntity.ok(updatedCustomer);
		}
		
//		/* PATCH */
//		@PatchMapping("/customers/{id}")
//		public ResponseEntity<Customer> patch(@RequestBody Customer newCustomer, @PathVariable (value = "id")  long id) {
//			
//			Customer customer = customerService.findCustomerById(id); 
//			if(customer==null) throw new CustomerNotFound();
//			
//			if(newCustomer.getFirstName()!=null)customer.setFirstName(newCustomer.getFirstName());
//			if(newCustomer.getLastName()!=null)customer.setLastName(newCustomer.getLastName());
//			if(newCustomer.getEmail()!=null)customer.setEmail(newCustomer.getEmail());
//			
//			//if(newUser.getUser_type()>2) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);//throw bad request
//			
//			Customer updatedCustomer = customerService.createCustomer(customer);
//			//implementing HATEOAS self link here
//			 Link selfLink = linkTo(CustomerController.class).slash("customers").slash(updatedCustomer.getId()).withSelfRel();
//			 updatedCustomer.add(selfLink);
//			return ResponseEntity.ok(updatedCustomer);
//		}

}
