package com.blo.crm.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blo.crm.model.AuthenticationBean;


@RestController
@RequestMapping(value = "/api/v1")
@CrossOrigin(origins ="http://localhost:3000") 
public class BasicAuthenticationController {

    @GetMapping(path = "/basicauth")
    public AuthenticationBean authenticate() {
        //throw new RuntimeException("Some Error has Happened! Contact Support at ***-***");
        return new AuthenticationBean("You are authenticated");
    }   
    /*NB: There is no authentication logic in authenticate() method. Why?
     *  Because, we already configured spring security, it auto protects all URLs with basic authentication. 
     *  A user would be able to get to /basicauth only if they provide the right credentials.
     */
}