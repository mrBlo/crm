package com.blo.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.blo.crm.service.MyUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private MyUserDetailsService myUserDetailsService;
	
	@Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }
	
	// for external auth
	@Bean
	 public AuthenticationProvider authProvider() {
		DaoAuthenticationProvider provider= new DaoAuthenticationProvider();
		provider.setUserDetailsService(myUserDetailsService);
		provider.setPasswordEncoder(passwordEncoderBean());
		return provider;
	}
	
	
	
	
	//Note: When you are using authentication, before executing the actual client request,
	//		the browser sends an OPTION request.
	// 		So we have to configure Spring Security to enable all OPTIONS requests
	
	 @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	        .csrf().disable()   
	        .authorizeRequests()
	        .antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
	                .anyRequest().authenticated()
	                .and()
	            .formLogin().and()
	            .httpBasic();
	    }


	 
	 
	 
	 
	 
	 
//	 @Bean
//	@Override
//	protected UserDetailsService userDetailsService() {
//		 List<UserDetails> users= new ArrayList<>();
//		 users.add(User.withDefaultPasswordEncoder()
//				 .username("user").password("password")
//				 .roles("USER")
//				 .build());
//		 return new InMemoryUserDetailsManager(users);
//		
//	}
	 
	 
	 
	 
	
	
	
}
