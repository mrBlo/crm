package com.blo.crm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class OpenApiConfig {

	  @Bean
	    public OpenAPI customOpenAPI() {
	        return new OpenAPI()
	                .components(new Components())
	                .info(new Info()
	                		.title("CRM Application API")
	                		.version("v1")
	                		.description(
	                        "This is a RESTful service for managing customers.")
	                		.contact(new Contact()
	                				.name("Yaw Afrifa")
	                				.email("myemail@yahoo.com")
	                				.url("http://crmapi.com/support")
	                				));
	      
	    }
	  
}
