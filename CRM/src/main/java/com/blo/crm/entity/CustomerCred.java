package com.blo.crm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name = "customer_cred")
@Data
public class CustomerCred {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="ID")
	private long id;
	
	@Column(name="USERNAME")
	@NotBlank(message="Username must not be empty")
    @Size(min=6, max=20, message = "Username must be between 6 and 20 characters long")
    @Pattern(regexp="^$|[a-zA-Z ]+$", message="Username must not include special characters.")
	private String username;
	
	@Column(name="PASSWORD")
	@NotBlank(message="Password must not be empty")
    @Size(min=10, max=20, message = "Password must be between 10 and 20 characters long")
	private String password;
	
//	@Column(name="USER_TYPE")
//	private int userType;
	

}
