package com.blo.crm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


import org.springframework.hateoas.RepresentationModel;

//import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Entity
@Data
@Table(name = "customer")
public class Customer extends RepresentationModel<Customer>{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	@Column(name="ID")
//	@Schema(description = "Unique identifier of the Customer.", 
//    example = "1", required = true)
	private Long id;
	
	 
//    @Schema(description = "First name of the customer.", 
//            example = "Jessica", required = true)
	@Column(name="FIRSTNAME")
	@NotBlank(message="Please provide a first name")
    @Pattern(regexp="^$|[a-zA-Z ]+$", message="First name must not include special characters.")
	private String firstName;
	
    
//    @Schema(description = "Last name of the customer.", 
//            example = "Alba", required = true)
	@Column(name="LASTNAME")
	@NotBlank(message="Please provide a last name")
    @Pattern(regexp="^$|[a-zA-Z ]+$", message="Last name must not include special characters.")
	private String lastName;
	
    
//    @Schema(description = "Email address of the customer.", 
//            example = "jessica@email.com", required = false)
	@Column(name="EMAIL")
	@NotBlank(message = "Please provide an email")
	@Email(message = "Please provide a valid email address")
	private String email;


	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Customer(Long id,String firstName,String lastName, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public Customer(String firstName,String lastName, String email) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	
	
}
