package com.blo.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blo.crm.entity.CustomerCred;


@Repository
public interface CustomerCredRepository extends JpaRepository<CustomerCred,Long> {

	
	CustomerCred findByUsername(String username);
	
	
	
}
