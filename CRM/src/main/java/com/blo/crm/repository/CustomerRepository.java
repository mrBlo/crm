package com.blo.crm.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.blo.crm.entity.Customer;

@RepositoryRestResource //use this instead of @Repository, in order to enable CORS
public interface CustomerRepository extends CrudRepository<Customer, Long>{

	//customizing findById method
	Customer findById(long id);  
	
	
}
