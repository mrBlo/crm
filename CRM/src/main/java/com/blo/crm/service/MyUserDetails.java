package com.blo.crm.service;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.blo.crm.entity.CustomerCred;

public class MyUserDetails implements UserDetails {

	private CustomerCred customerCred;
	
	
	public MyUserDetails(CustomerCred customerCred) {
		this.customerCred = customerCred;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		//making all principals as Users
		return Collections.singleton(new SimpleGrantedAuthority("USER"));
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return customerCred.getPassword();
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return customerCred.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
