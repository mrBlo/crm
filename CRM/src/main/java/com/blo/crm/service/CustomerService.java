package com.blo.crm.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blo.crm.entity.Customer;
import com.blo.crm.entity.exception.CustomerNotFound;
import com.blo.crm.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;
	
	public List<Customer> getAllCustomers(){
	 List<Customer>allCustomers= new ArrayList<Customer>();
	 customerRepository.findAll().forEach(
			 customer->{
				 	allCustomers.add(customer);
			 });
	 
	 return allCustomers; 
	}
	
	public Customer findCustomerById(long id) {
		Customer customer =this.customerRepository.findById(id);
		if(customer==null) throw new CustomerNotFound();		
		return customer;	
	}
	
	public Customer createCustomer(Customer customer) {
		return this.customerRepository.save(customer); 
	}
	
	//deleteUser
	public void deleteCustomer(Customer customer) {
		 this.customerRepository.delete(customer);
	}
	
	
	
	
	
}
