package com.blo.crm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.blo.crm.entity.CustomerCred;
import com.blo.crm.repository.CustomerCredRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	CustomerCredRepository customerCredRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		 CustomerCred userDetails= customerCredRepository.findByUsername(username);
		 if(userDetails==null) throw new UsernameNotFoundException("Invalid User Credentials");
		 
		return new MyUserDetails(userDetails);
	}

}
